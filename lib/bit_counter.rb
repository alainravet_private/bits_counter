class BitCounter

  # return the number of '1' bits found in the byte
  # @param [Number] byte # MUST be in 0..255.
  # caveat : Don't expect an error if the byte value is invalid.
  def self.nof_1s_in(byte)
    CACHED_ONES_COUNTER[byte]
  end

private

  CACHED_ONES_COUNTER = 0.upto(255).collect {|byte| byte.to_s(2).count('1')}

  def CACHED_ONES_COUNTER.[](index)
    CACHED_ONES_COUNTER.fetch index
  end

end