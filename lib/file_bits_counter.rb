require_relative 'bit_counter'

class FileBitsCounter

  attr_reader :nof_1s, :nof_0s, :nof_bytes

  def initialize(io)
    @io = io
    @nof_bytes, @nof_1s , @nof_0s = 0, 0, 0
    read_file_and_count_bits
  end


private

  def read_file_and_count_bits
    File.open(@io, 'rb') do |file|
      file.each_byte do |ch|
        @nof_bytes += 1
        @nof_1s    += BitCounter.nof_1s_in(ch)
      end
    end
    @nof_0s = 8 * @nof_bytes - @nof_1s
  end

end