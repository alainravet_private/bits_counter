requirements : Ruby 1.9.* or Ruby 2.*

Installation 
	$ git clone git@bitbucket.org:alainravet/bits_counter.git
 	$ cd bits_counter/
	$ ./count_bits <path_to_a_file>
		File: count_bits_irb.rb
		  found 959 bits set to 1
		  found 1321 bits set to 0


Usage : in the console
    $ count_bits  /a/valid/file/path
  or
    $ /path/to/count_bits  /a/valid/file/path

Usage : in IRB
    $ cd <project-root>
    $ irb -r 'count_bits_irb.rb'
        count_bits '/a/valid/file/path'

---------------------------------------------------------------

DESCRIPTION OF THE PROBLEM
    Case A: Bit counter
        Assume there is a large file, for example a jpg, saved on disk as a set of zeros and ones. The question then is,
        how many zeros and how many ones are present in the file.
        We need to call a function such as (in IRB): count_bits "/path/to/some/file.jpg"
        Example of output on stdout:
            found 456 bits set to 1
            found 128 bits set to 0


---------------------------------------------------------------
TODO :
    better error messages

---------------------------------------------------------------

RUN the tests :
    $ cd <source root>
    $ for file in test/*.rb; do ruby $file; done
or
    $ gem install guard guard-minitest
    $ guard


PLAN :
    - read the file byte by byte
    - obtain number of 1/0 of each byte, and increment the appropriate counters


