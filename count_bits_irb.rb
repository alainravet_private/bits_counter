require_relative 'lib/file_bits_counter'

module Kernel

  def count_bits(path)
    File.open(path) do |file|
      c = FileBitsCounter.new(file)
      puts "File: #{path}"
      puts "  found #{c.nof_1s} bits set to 1"
      puts "  found #{c.nof_0s} bits set to 0"
    end

  end
end