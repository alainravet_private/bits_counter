require_relative '../lib/bit_counter'

require 'minitest/autorun'

class BitCounterTest < MiniTest::Unit::TestCase

  def test___nof_1_in___obtains_the_number_of_1_in_a_byte
    BitCounter.nof_1s_in( 0b0000_0000 ).must_equal 0
    BitCounter.nof_1s_in( 0b0000_0001 ).must_equal 1
    BitCounter.nof_1s_in( 0b0000_0010 ).must_equal 1
    BitCounter.nof_1s_in( 0b0000_0101 ).must_equal 2
    BitCounter.nof_1s_in( 0b0111_1111 ).must_equal 7
    BitCounter.nof_1s_in( 0b1111_1111 ).must_equal 8

    BitCounter.nof_1s_in( 0b0110_0001 ).must_equal 3
    BitCounter.nof_1s_in( 0b0110_0010 ).must_equal 3
    BitCounter.nof_1s_in( 0b0110_0011 ).must_equal 4
  end
end
