require_relative '../lib/file_bits_counter'

require 'minitest/autorun'

class FileBitsCounterTest < MiniTest::Unit::TestCase
  require 'tempfile'
  def setup
  # abc => 1100001 1100010 1100011
  #     == 10 '1'   # 3 + 3 + 4
  #     == 14 '0'   # 3*8 - (3 + 3 + 4)
    @file = Tempfile.new('foo')
    @file.write 'abc'
    @file.close

  end
  def teardown
    @file.unlink
  end

  def test_setup
    'abc'.bytes.to_a.
        map{|byte| BitCounter.nof_1s_in(byte)}.
        reduce(:+).must_equal 3 + 3 + 4
  end

  def test___length
    FileBitsCounter.new(@file).nof_bytes.must_equal 3
  end

  def test___nof_1s
    FileBitsCounter.new(@file).nof_1s.must_equal 10
  end

  def test___nof_0s
    FileBitsCounter.new(@file).nof_0s.must_equal 24 - 10
  end

  def test__case_empty_file
    file = Tempfile.new('foo')
    file.close
    FileBitsCounter.new(file).nof_bytes .must_equal 0
    FileBitsCounter.new(file).nof_1s    .must_equal 0
    FileBitsCounter.new(file).nof_0s    .must_equal 0
  end
end
